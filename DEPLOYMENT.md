# Deploying using opentofu:

## Prerequisites (on local machine)
- [Angular and it's dependencies](https://angular.io/guide/setup-local)
- [opentofu](https://opentofu.org/docs/intro/install/)

## Steps
1. First, download your `cloud.yaml` file from openstack by navigating to "API Access" from the sidebar and choosing "Openstack clouds.yaml File" from the drop down menu labeled "Download OpenStack RC Files". 
2. Clone [project](https://gitlab.com/kshuta/concurrency-vis) and `cd` into `./infra`.
3. Move the `cloud.yaml` file from step 1 into current directory (`./infra`). Also set an environment variable called `$OS_PASSWORD` and set it to the password to your openstack account.
4. Run the following commands to spin up virtual machine:
```
echo "public_key = $(cat ~/.ssh/id_rsa.pub)" > main.tfvars
tofu init
tofu apply --vars-file=main.tfvars --auto-approve
```
The last command will output an IP address of the vm. For convenience, declare an 
environment variable called `ENDPOINT` and set the ip address as the value.
5. ssh into the vm using the IP address from previous step and user `ubuntu`
```
# example
ssh ubuntu@$ENDPOINT
```

6. run the following commands in virtual machine
```
chown -R ubuntu /app/
```
7. Exit out of the virtual machine 
8. Go back to the cloned project in your local machine, and edit the `angular/src/environments/environment.prod.ts` file. Swap out the URL with the IP address of the VM. **Don't forget the :8080 port number**. 

Similarly, edit line 55 of `./angular/src/app/unisex-past-runs/unisex-past-runs.component.html` where it includes the link to the visualization. Change the local host to the IP address of the VM. **Don't modify the :8888 port number and beyond**.

Then run the following command:
```
task deploy-angular
```
9. ssh back into the virtual machine and perform the following commands:
```
cd /app
task run-app
```

You should be all set!!

# Deploying without opentofu:

1. Create a VM on openstack (ubuntu flavor).

2. ssh into the VM

3. Install depenencies (as root). The dependencies are the same those listed in DEVELOPMENT.md, but 
you can refer to the `infra/userdata.sh` for specific scripts to install them.

- Angular (npm)
- Go
- Docker
- Taskfile
- The project (https://gitlab.com/kshuta/concurrency-vis)

4. Find the hard coded IP address of the virtual machine (noted as public IP address), and swap out the IP address of `angular/src/environments/environment.prod.ts` and `./angular/src/app/unisex-past-runs/unisex-past-runs.component.html` (around line 55) to the virtual machine's public IP address.

5. Run `sudo task build-angular` to build the angular project.

6. run `sudo task run-app` from the project root directory.

7. Access http://<ipadderss> to see if the app is running!

## Debugging

- The logs for the go server are located under `go/logs/`, the logs for the proxy are located under `proxy/*.log`, and the logs for the database are accessible through `docker logs stats-db`.
- The postgres credentials are located in the `.env` file at the root of the project directory. copy them as necessary.
- The `proxy/grpwebserver` only works on ubuntu. Install platform specific binaries if you wish to run the app on a different operating system.

