#!/bin/bash

wget https://go.dev/dl/go1.22.2.linux-amd64.tar.gz
rm -rf /usr/local/go 
sudo tar -C /usr/local -xzf go1.22.2.linux-amd64.tar.gz
echo export PATH=$PATH:/usr/local/go/bin >> ~/.bashrc

git clone https://gitlab.com/kshuta/concurrency-vis.git /app
snap install task --classic

cd /app

# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl -y
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y

mkdir /app/go/logs

