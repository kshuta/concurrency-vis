# Define required providers
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.53.0"
    }
  }
}

# Configure the OpenStack Provider
provider "openstack" {
  cloud = "openstack"
}

# Create a web server
resource "openstack_compute_instance_v2" "conc_viz_server" {
  name = "conc_viz_server"
  image_id = "f9b63d32-817e-4e8a-a9f8-c42a6e760704" # ubuntu image id on openstack
  key_pair = openstack_compute_keypair_v2.conc_viz_keypair.id
  security_groups = ["default"]
  flavor_id = "4"

  user_data = file("userdata.sh")
}

resource "openstack_compute_keypair_v2" "conc_viz_keypair" {
  name       = "conc-viz-keypair"
  public_key = var.public_key
}

variable "public_key" {
  type = string
  description = "The public key used for the keypair assigned to the instance"
}


