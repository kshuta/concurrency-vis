{{ define "main.js"}}


let ctx;
let data;
let config;
{{ $cat_num := .UnisexRun.Options.NumMales}}
{{ range .UnisexRun.Stats }}

{/* Eat vs Wait time for each characters*/}
ctx = document.getElementById('pie-chart-{{ .Id }}');
data = {
    labels: [
          'Total eat time',
          'Total wait time',
        ],
    datasets: [{
      label: 'ns',
          data: [ {{ .TimeUsingTotal }}, {{ .WaitTimeTotal }}],
          backgroundColor: [
                  'rgb(255, 99, 132)',
                  'rgb(54, 162, 235)',
                ],
          hoverOffset: 4
        }]
};
config = {
    type: 'pie',
    data: data,
    options: {
      responsive: true,
      plugins: {
        title: {
          display: true,
          text: "{{ if (lt .Id $cat_num) }} Cat: {{ else }} Mouse: {{ end }} {{ modulo .Id $cat_num}}"
        },
        legend: {
          display: false,
        }
      }
    }
};
new Chart(ctx, config)

{/* ================ */}
{{ end }} 

{/* average time spent waiting/eating */}
ctx = document.getElementById('avg-eat-and-wait-bar');

data = {
    labels: ["Eating", "Waiting"],
    datasets: [{
          label: 'Cats',
          data: [{{ .CatAverage.EatTime }}, {{.CatAverage.WaitTime}}],
          backgroundColor: [
                  'rgb(255, 99, 132)',
                  'rgb(255, 99, 132)',
                ],
          borderColor: [
                  'rgb(255, 99, 132)',
                  'rgb(255, 99, 132)',
                ],
          borderWidth: 1
        },
      {
          label: 'Mice',
          data: [{{ .MouseAverage.EatTime }}, {{.MouseAverage.WaitTime}}],
          backgroundColor: [
                  'rgb(54, 162, 235)',
                  'rgb(54, 162, 235)',
                ],
          borderColor: [
                  'rgb(54, 162, 235)',
                  'rgb(54, 162, 235)',
                ],
          borderWidth: 1
        }
    ]
};
config = {
    type: 'bar',
    data: data,
    options: {
      responsive: true,
      plugins: {
        title: {
          display: true,
          text: "Average eat and wait time for cats and mice",
        },
        legend: {
          display: true,
        }
      }
    }
};
new Chart(ctx, config);

{/* ================ */}

ctx = document.getElementById('bowl-utilization');

data = {
    labels: ["Spent", "Capacity"],
    datasets: [{
          label: 'Utilization',
          data: [ {{ .CycleCount }}, {{ minus64 .IdealCycleCount .CycleCount }}],
          backgroundColor: [
                  'rgb(255, 99, 132)',
                  'rgb(54, 162, 235)',
                ],
          borderWidth: 1
        },
    ]
};
config = {
    type: 'pie',
    data: data,
    options: {
      responsive: true,
      plugins: {
        title: {
          display: false,
        },
        legend: {
          display: true,
        }
      }
    }
};
new Chart(ctx, config);
{{end}}
