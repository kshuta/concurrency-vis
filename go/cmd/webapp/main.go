package main

import (
	"database/sql"
	"fmt"
	"html/template"
	"log"
	"log/slog"
	"net/http"
	"strconv"

	"github.com/Netflix/go-env"
	"github.com/joho/godotenv"
	"github.com/julienschmidt/httprouter"
	"gitlab.com/kshuta/concurrency-vis/css"
	"gitlab.com/kshuta/concurrency-vis/db"

	_ "github.com/lib/pq"
)

type Average struct {
	EatTime  int64
	WaitTime int64
}

type templateData struct {
	Title           string
	UnisexRun       *css.UnisexRun
	CatAverage      Average
	MouseAverage    Average
	CycleCount      int64
	IdealCycleCount int64
}

type pgConfig struct {
	User     string `env:"PGUSER"`
	Password string `env:"PGPASS"`
	Host     string `env:"PGHOST"`
	Port     string `env:"PGPORT"`
	DBName   string `env:"PG_DBNAME"`
}

var runModel *db.RunModel

func mx(a, b int32) int32 {
	if a > b {
		return a
	}
	return b
}

func minus[V int64 | int32 | int](a, b V) string {
	return strconv.FormatInt(int64(a-b), 10)
}
func modulo(a, b int32) string {
	return strconv.FormatInt(int64(a%b), 10)
}
func divide(a, b int32) string {
	return strconv.FormatInt(int64(a/b), 10)
}

func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprintln(w, "Welcome!")
}

func Chart(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	funcMap := template.FuncMap{
		"minus":   minus[int32],
		"minus64": minus[int64],
		"modulo":  modulo,
		"divide":  divide,
		"mx":      mx,
	}
	ts, err := template.New("base.tmpl.html").Funcs(funcMap).ParseFS(Files, "static/base.tmpl.html", "static/js/main.js", "static/css/style.css")
	if err != nil {
		slog.Error("error parsing template", "err", err.Error())
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	slog.Info("gettign stats", "savecode", ps.ByName("savecode"), "timestamp", ps.ByName("timestamp"))

	unisexRun, err := runModel.GetUnisexRun(ps.ByName("savecode"), ps.ByName("timestamp"))
	if err != nil {
		slog.Error("error getting unisex run", "err", err.Error())
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return
	}

	var catWaitAvg int64 = 0
	var catEatAvg int64 = 0
	var mouseWaitAvg int64 = 0
	var mouseEatAvg int64 = 0

	var cycleCount int64 = 0

	for i, stat := range unisexRun.Stats {
		if int32(i) < unisexRun.Options.NumMales {
			// cats
			catEatAvg += stat.TimeUsingTotal
			catWaitAvg += stat.WaitTimeTotal
		} else {
			// mice
			mouseEatAvg += stat.TimeUsingTotal
			mouseWaitAvg += stat.WaitTimeTotal
		}
		cycleCount += stat.TimesUsed
	}

	idealcc := (unisexRun.Options.Duration * int64(unisexRun.Options.MaxAllowed)) / ((unisexRun.Options.MaleTime + unisexRun.Options.FemaleTime) / 2)

	slog.Info("max allowed", "max", unisexRun.Options.MaxAllowed)

	templateData := templateData{
		Title:     "Visualization",
		UnisexRun: unisexRun,
		CatAverage: Average{
			WaitTime: catWaitAvg / int64(unisexRun.Options.NumMales),
			EatTime:  catEatAvg / int64(unisexRun.Options.NumMales),
		},
		MouseAverage: Average{
			WaitTime: mouseWaitAvg / int64(unisexRun.Options.NumFemales),
			EatTime:  mouseEatAvg / int64(unisexRun.Options.NumFemales),
		},
		CycleCount:      cycleCount,
		IdealCycleCount: idealcc,
	}

	err = ts.Execute(w, templateData)
	if err != nil {
		log.Print(err.Error())
		http.Error(w, "Internal Server Error", http.StatusInternalServerError)
		return

	}
}

func main() {
	err := godotenv.Load()
	if err != nil {
		slog.Error(err.Error())
		return
	}

	var pgconfig pgConfig
	_, err = env.UnmarshalFromEnviron(&pgconfig)
	if err != nil {
		slog.Error(err.Error())
		return
	}

	slog.Info("config", "pgconfig", pgconfig)

	connStr := fmt.Sprintf(
		"user=%s password=%s host=%s port=%s sslmode=disable dbname=%s",
		pgconfig.User,
		pgconfig.Password,
		pgconfig.Host,
		pgconfig.Port,
		pgconfig.DBName,
	)
	dbconn, err := sql.Open("postgres", connStr)
	if err != nil {
		slog.Error(err.Error())
		return
	}
	runModel = db.NewRunModel(dbconn)
	router := httprouter.New()
	router.GET("/", Index)
	router.GET("/chart/:savecode/:timestamp", Chart)
	router.Handler("GET", "/static/", http.FileServerFS(Files))
	//router.ServeFiles("/static/*filepath", http.FileServerFS(Files))
	log.Fatal(http.ListenAndServe(":8888", router))
}
