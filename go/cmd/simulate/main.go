package main

import (
	"database/sql"
	"fmt"
	"log/slog"
	"math/rand"
	"os"
	"time"

	"github.com/Netflix/go-env"
	"github.com/joho/godotenv"
	"gitlab.com/kshuta/concurrency-vis/css"
	"gitlab.com/kshuta/concurrency-vis/db"

	_ "github.com/lib/pq"
)

type Environment struct {
	PgConfig struct {
		User         string `env:"PGUSER"`
		Password     string `env:"PGPASS"`
		Host         string `env:"PGHOST"`
		Port         string `env:"PGPORT"`
		DBName       string `env:"PG_DBNAME"`
		MAXOPENCONNS int    `env:"PG_MAXOPENCONNS,default=50"`
	}

	Duration            int64 `env:"DURATION,default=3"`
	SizeLowerRange      int32 `env:"SIZE_LOWER_RANGE,default=2"`
	SizeUpperRange      int32 `env:"SIZE_UPPER_RANGE,default=20"`
	SizeIncrement       int32 `env:"SIZE_INCREMENT,default=1"`
	ThinkTimeLowerRange int64 `env:"THINK_TIME_LOWER_RANGE,default=0"`
	ThinkTimeUpperRange int64 `env:"THINK_TIME_UPPER_RANGE,default=50000"`
	ThinkTimeIncrement  int64 `env:"THINK_TIME_INCREMENT,default=100"`
	EatTimeLowerRange   int64 `env:"EAT_TIME_LOWER_RANGE,default=0"`
	EatTimeUpperRange   int64 `env:"EAT_TIME_UPPER_RANGE,default=50000"`
	EatTimeIncrement    int64 `env:"EAT_TIME_INCREMENT,default=100"`
	SampleRate          int   `env:"SAMPLE_RATE,default=100"`
	Workers             int   `env:"WORKERS,default=20"`
	JobLimit            int   `env:"JOB_LIMIT,default=5000"`
	Solution            int32 `env:"SOLUTION,default=0"`
}

func main() {
	err := godotenv.Load()
	if err != nil {
		slog.Error("failed to load .env variables", "err", err.Error())
		return
	}

	var envVars Environment
	_, err = env.UnmarshalFromEnviron(&envVars)
	if err != nil {
		slog.Error("failed to unmarshal env variables", "err", err.Error())
		return
	}

	slog.Info("envVars", "envVars", envVars)

	connStr := fmt.Sprintf(
		"user=%s password=%s host=%s port=%s sslmode=disable dbname=%s",
		envVars.PgConfig.User,
		envVars.PgConfig.Password,
		envVars.PgConfig.Host,
		envVars.PgConfig.Port,
		envVars.PgConfig.DBName,
	)
	dbconn, err := sql.Open("postgres", connStr)
	if err != nil {
		slog.Error("failed to open database", "err", err.Error())
		return
	}
	dbconn.SetMaxOpenConns(envVars.PgConfig.MAXOPENCONNS)

	runModel := db.NewRunModel(dbconn)

	saveCode, err := runModel.GetSaveCode()
	if err != nil {
		slog.Error("failed to get savecode", "err", err)
		return
	}

	slog.Info("savecode", "savecode", saveCode)

	log := slog.New(slog.NewTextHandler(os.Stdout, nil))
	jobs := make(chan *css.DPOptions, 50)
	doneChan := make(chan int, envVars.JobLimit)
	for w := 0; w < envVars.Workers; w++ {
		go worker(jobs, doneChan, log, runModel)
	}

	count := 0
	for size := envVars.SizeLowerRange; size <= envVars.SizeUpperRange; size += envVars.SizeIncrement { // 18
		for thinkTime := envVars.ThinkTimeLowerRange; thinkTime <= envVars.ThinkTimeUpperRange; thinkTime += envVars.ThinkTimeIncrement {
			for eatTime := envVars.EatTimeLowerRange; eatTime <= envVars.EatTimeUpperRange; eatTime += envVars.EatTimeIncrement {
				if count >= envVars.JobLimit {
					goto done
				}
				options := &css.DPOptions{
					Choice:       envVars.Solution,
					Duration:     envVars.Duration * 1e9,
					Size:         size,
					TimeThinking: thinkTime,
					TimeEating:   eatTime,
					Variation:    int64(0),
					SaveCode:     saveCode,
				}
				if rand.Intn(envVars.SampleRate) == 0 {
					log.Info("sending job", "options", options)
					jobs <- options
					count += 1
				}
			}
		}
	}
done:
	log.Info("closing doneChan and jobs", "count", count)
	close(jobs)

	for i := 0; i < count; i++ {
		<-doneChan
	}
	runModel.Close()
}

func worker(jobs <-chan *css.DPOptions, doneChan chan<- int, log *slog.Logger, runModel *db.RunModel) {
	for j := range jobs {
		fmt.Println("philosophizing")
		c, err := css.Philosophize(j, log)
		if err != nil {
			slog.Error("failed to call philosophize", "err", err.Error())
			goto done
		}
		stats := make([]*db.PhilosopherStat, j.Size)
		for i := range stats {
			stats[i] = &db.PhilosopherStat{Last: time.Now().UnixNano()}
		}
		for v := range c {
			stats[v.Id].Update(v)
		}
		if err := runModel.StoreDp(*j, stats); err != nil {
			slog.Error("failed to store dp stats in db", "error", err.Error())
			goto done
		}
	}
done:
	doneChan <- 1
}
