package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"log/slog"
	"net"
	"os"
	"os/signal"
	"time"

	env "github.com/Netflix/go-env"
	"github.com/joho/godotenv"
	"gitlab.com/kshuta/concurrency-vis/css"
	"gitlab.com/kshuta/concurrency-vis/db"

	_ "github.com/lib/pq"
	"google.golang.org/grpc"
)

var dbconn *sql.DB
var runModel *db.RunModel

var port = flag.String("port", "9090", "Port number to listen for connections.")

type pgConfig struct {
	User     string `env:"PGUSER"`
	Password string `env:"PGPASS"`
	Host     string `env:"PGHOST"`
	Port     string `env:"PGPORT"`
	DBName   string `env:"PG_DBNAME"`
}

func main() {
	flag.Parse()

	err := godotenv.Load()
	if err != nil {
		slog.Error(err.Error())
		return
	}

	var pgconfig pgConfig
	_, err = env.UnmarshalFromEnviron(&pgconfig)
	if err != nil {
		slog.Error("failed to unmarshal config from environment", "err", err.Error())
		return
	}

	connStr := fmt.Sprintf(
		"user=%s password=%s host=%s port=%s sslmode=disable dbname=%s",
		pgconfig.User,
		pgconfig.Password,
		pgconfig.Host,
		pgconfig.Port,
		pgconfig.DBName,
	)
	dbconn, err = sql.Open("postgres", connStr)
	if err != nil {
		slog.Error("failed to open postgres", "err", err.Error())
		return
	}
	runModel = db.NewRunModel(dbconn)

	lis, err := net.Listen("tcp", ":"+*port)
	if err != nil {
		slog.Error(fmt.Sprintf("failed to listen: %v", err))
		return
	}
	grpcServer := grpc.NewServer()
	css.RegisterCSSServer(grpcServer, &cssServer{})
	slog.Info("Serving grpc server")

	finished := make(chan struct{})
	go func() {
		c := make(chan os.Signal, 1)
		signal.Notify(c, os.Interrupt)
		<-c
		grpcServer.GracefulStop()
		slog.Info("Gracefully shutdown server.")
		if err := dbconn.Close(); err != nil {
			slog.Error("error closing database: %v\n", err)
		}
		slog.Info("Closed database connection.")
		close(finished)
	}()
	if err := grpcServer.Serve(lis); err != nil {
		slog.Error(err.Error())
		return
	}
	<-finished
}

type cssServer struct{}

func (*cssServer) NewSaveCode(context.Context, *css.Empty) (*css.SaveCode, error) {
	slog.Info("Getting new save code")
	saveCode, err := runModel.GetSaveCode()
	slog.Info("new save code", "saveCode", saveCode.SaveCode)
	return saveCode, err
}

func (*cssServer) ValidateSaveCode(_ context.Context, code *css.SaveCode) (*css.SaveCodeValidationResponse, error) {
	slog.Info("Validating save code", "saveCode", code.SaveCode)
	exists, err := runModel.ValidateSaveCode(code.SaveCode)
	if err != nil {
		return nil, err
	}
	return &css.SaveCodeValidationResponse{Valid: exists}, nil
}

func (*cssServer) BoundedBuffer(o *css.BufferOptions, s css.CSS_BoundedBufferServer) error {
	slog.Info("Entered BoundedBuffer")
	stats := make([]*db.BufferStat, o.NumConsumers+o.NumProducers, o.NumProducers+o.NumConsumers)
	for i := range stats {
		stat := &db.BufferStat{Last: time.Now().UnixNano()}
		if i < int(o.NumProducers) {
			stat.ActorType = "producer"
		} else {
			stat.ActorType = "consumer"
		}
		stats[i] = stat
	}
	c := css.BoundedBuffer(*o)
	for state := range c {
		stats[state.Id].Update(state)
		if err := s.Send(state); err != nil {
			return err
		}
	}
	slog.Info("Exiting BoundedBuffer")
	return runModel.SaveBufferToDB(*o, stats)
}

func (*cssServer) GetBufferStats(_ context.Context, code *css.SaveCode) (*css.BufferStats, error) {
	res, err := dbconn.Query(`select * from buffer_run br where br.session_id = $1 order by br.id desc`, code.SaveCode)
	if err != nil {
		return nil, err
	}

	// Grab runs
	var runs = make(map[int64]*css.BufferRun)
	for res.Next() {
		var id int64
		var run = new(css.BufferRun)
		run.Options = new(css.BufferOptions)
		run.Options.SaveCode = new(css.SaveCode)
		var timeValue time.Time
		if err := res.Scan(
			&id,
			&(run.Options.SaveCode.SaveCode),
			&timeValue,
			&(run.Options.Duration),
			&(run.Options.BufferSize),
			&run.Options.NumProducers,
			&(run.Options.NumConsumers),
			&(run.Options.ProduceTime),
			&(run.Options.ConsumeTime),
			&(run.Options.Variation),
			&(run.Options.SleepTime),
		); err != nil {
			return nil, err
		}
		run.Timestamp = timeValue.UnixNano()
		runs[id] = run
	}

	// Grab the individual stats.
	res, err = dbconn.Query(`select bs.* from buffer_run br join buffer_stat bs on br.id = bs.buffer_run_id where br.session_id = $1 order by bs.buffer_run_id, bs.buffer_number;`, code.SaveCode)
	if err != nil {
		return nil, err
	}
	for res.Next() {
		var id int64 = -1
		var throwaway string
		var stat = new(css.BufferRunStat)
		if err := res.Scan(
			&id,
			&stat.BufferStatNum,
			&throwaway,
			&stat.Actions,
			&stat.ActionTimeMax,
			&stat.WaitTimeMax,
			&stat.ActionTimeMin,
			&stat.WaitTimeMin,
			&stat.ActionTimeTotal,
			&stat.WaitTimeTotal,
		); err != nil {
			panic(err)
		}
		runs[id].Stats = append(runs[id].Stats, stat)
	}

	result := new(css.BufferStats)
	for _, v := range runs {
		result.BufferRuns = append(result.BufferRuns, v)
	}
	return result, nil
}

func (*cssServer) Unisex(o *css.UnisexOptions, s css.CSS_UnisexServer) error {
	slog.Info("Hit Unisex endpoint")
	stats := make([]*db.UnisexStat, o.NumMales+o.NumFemales)
	for i := range stats {
		stat := &db.UnisexStat{Last: time.Now().UnixNano()}
		if i < int(o.NumMales) {
			stat.ActorType = "male"
		} else {
			stat.ActorType = "female"
		}
		stats[i] = stat
	}
	var c <-chan *css.UnisexState
	if o.Solution == css.UnisexOptions_FIRST {
		c = css.FirstUnisex(*o)
	} else {
		c = css.NoStarveUnisex(*o)
	}
	for value := range c {
		if value.Id >= 0 {
			stats[value.Id].Update(value)
		}
		if err := s.Send(value); err != nil {
			return err
		}
	}
	return storeUnisex(*o, stats)
}

func (*cssServer) GetUnisexStats(_ context.Context, code *css.SaveCode) (*css.UnisexStats, error) {
	return runModel.GetUnisexStats(code.SaveCode)
}

func (p *cssServer) ReadWrite(o *css.RWOptions, s css.CSS_ReadWriteServer) error {
	slog.Info("Entering ReadWrite")

	stats := make([]*ReadWriteStat, o.NumReaders+o.NumWriters)
	for i := range stats {
		stat := new(ReadWriteStat)
		stat.Last = time.Now().UnixNano()
		if i < int(o.NumReaders) {
			stat.ActorType = "reader"
		} else {
			stat.ActorType = "writer"
		}
		stats[i] = stat
	}

	c := css.ReadWrite(o)

	for state := range c {
		temp := state
		stats[temp.Id].update(temp)
		if err := s.Send(&temp); err != nil {
			slog.Debug("Canceled, finishing out channel")
			go func() {
				for range c {
				}
				slog.Debug("Channel exhausted")
			}()
			return err
		}
	}
	err := saveRWToDB(*o, stats)
	return err
}

func (stat *ReadWriteStat) update(state css.RWState) {
	delta := state.Time - stat.Last

	var statistic *db.Statistic
	if state.State == css.RWState_END_ACTION {
		statistic = &stat.ActionTime
		stat.Cycles++
	} else if state.State == css.RWState_READING || state.State == css.RWState_WRITING {
		statistic = &stat.WaitTime
	}

	stat.Last = state.Time
	if statistic == nil {
		return
	}
	statistic.Total += delta
	if statistic.Min == 0 || delta < statistic.Min {
		statistic.Min = delta
	}
	if delta > statistic.Max {
		statistic.Max = delta
	}
}

func saveRWToDB(o css.RWOptions, stats []*ReadWriteStat) error {
	tx, err := dbconn.Begin()
	if err != nil {
		return err
	}

	row := tx.QueryRow(
		`insert into rw_run (session_id, timestamp, duration, solution, num_readers, num_writers, read_time, write_time, variation, read_sleep, write_sleep) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) returning id`,
		o.SaveCode.SaveCode,
		time.Now(),
		o.Duration,
		o.Solution,
		o.NumReaders,
		o.NumWriters,
		o.ReadDuration,
		o.WriteDuration,
		o.Variation,
		o.ReadSleep,
		o.WriteSleep,
	)

	var id int32

	if err := row.Scan(&id); err != nil {
		return err
	}

	for i, v := range stats {
		_, err := tx.Exec(
			`insert into rw_stat (rw_run_id, rw_number, actor_type, cycles, max_action_time, max_wait_time, min_action_time, min_wait_time, total_action_time, total_wait_time) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`,
			id,
			i,
			v.ActorType,
			v.Cycles,
			v.ActionTime.Max,
			v.WaitTime.Max,
			v.ActionTime.Min,
			v.WaitTime.Min,
			v.ActionTime.Total,
			v.WaitTime.Total,
		)
		if err != nil {
			panic(err)
		}
	}

	return tx.Commit()
}

func (*cssServer) GetRWStats(ctx context.Context, code *css.SaveCode) (*css.RWStats, error) {
	res, err := dbconn.Query(`select * from rw_run rwr where rwr.session_id = $1 order by rwr.id desc`, code.SaveCode)
	if err != nil {
		return nil, err
	}

	// Grab runs
	var runs = make(map[int64]*css.RWRun)
	for res.Next() {
		var id int64
		var run = new(css.RWRun)
		run.Options = new(css.RWOptions)
		run.Options.SaveCode = new(css.SaveCode)
		var timeValue time.Time
		if err := res.Scan(
			&id,
			&(run.Options.SaveCode.SaveCode),
			&timeValue,
			&(run.Options.Duration),
			&(run.Options.Solution),
			&run.Options.NumReaders,
			&(run.Options.NumWriters),
			&(run.Options.ReadDuration),
			&(run.Options.WriteDuration),
			&(run.Options.Variation),
			&(run.Options.ReadSleep),
			&(run.Options.WriteSleep),
		); err != nil {
			return nil, err
		}
		run.Timestamp = timeValue.UnixNano()
		runs[id] = run
	}

	// Grab the individual stats.
	res, err = dbconn.Query(`select rws.* from rw_run rwr join rw_stat rws on rwr.id = rws.rw_run_id where rwr.session_id = $1 order by rws.rw_run_id, rws.rw_number;`, code.SaveCode)
	if err != nil {
		return nil, err
	}
	for res.Next() {
		var id int64 = -1
		var stat = new(css.RWRunStat)
		if err := res.Scan(
			&id,
			&stat.ReadWriteNumber,
			&stat.ActorType,
			&stat.Actions,
			&stat.ActionTimeMax,
			&stat.WaitTimeMax,
			&stat.ActionTimeMin,
			&stat.WaitTimeMin,
			&stat.ActionTimeTotal,
			&stat.WaitTimeTotal,
		); err != nil {
			panic(err)
		}
		runs[id].Stats = append(runs[id].Stats, stat)
	}

	result := new(css.RWStats)
	for _, v := range runs {
		result.RwRuns = append(result.RwRuns, v)
	}
	return result, nil
}

func (p *cssServer) Philosophize(o *css.DPOptions, s css.CSS_PhilosophizeServer) error {
	log := slog.New(slog.NewTextHandler(os.Stdout, nil))
	log.Info("Entering Dining Philosophers")
	c, err := css.Philosophize(o, log)
	if err != nil {
		return fmt.Errorf("error in philosophize: %w", err)
	}
	stats := make([]*db.PhilosopherStat, o.Size)
	for i := range stats {
		stats[i] = &db.PhilosopherStat{Last: time.Now().UnixNano()}
	}
	for v := range c {
		stats[v.Id].Update(v)
		log.Debug("updating status", "philospherId", v.Id, "forkID", v.ForkId, "status", v.State)
		if err := s.Send(v); err != nil {
			return err
		}
	}
	log.Info("channel closed")
	err = storeDp(*o, stats)
	if err != nil {
		log.Error("error in saving dp:" + err.Error())
	}
	log.Info("Exiting Dining Philosophers")
	return err
}

func (*cssServer) GetDPStats(_ context.Context, code *css.SaveCode) (*css.DPStats, error) {
	slog.Info("Retrieving DP stats")
	res, err := dbconn.Query(`select * from dp_run dp where dp.session_id = $1 order by dp.id desc`, code.SaveCode)
	if err != nil {
		return nil, err
	}

	// Grab runs
	var runs = make(map[int64]*css.DPRun)
	for res.Next() {
		var id int64
		var run = new(css.DPRun)
		run.Options = new(css.DPOptions)
		run.Options.SaveCode = new(css.SaveCode)
		var timeValue time.Time
		if err := res.Scan(
			&id,
			&(run.Options.SaveCode.SaveCode),
			&timeValue,
			&(run.Options.Duration),
			&(run.Options.Size),
			&run.Options.TimeThinking,
			&(run.Options.TimeEating),
			&(run.Options.Variation),
			&(run.Deadlocked),
			&(run.Options.Choice),
		); err != nil {
			return nil, err
		}
		run.Timestamp = timeValue.UnixNano()
		runs[id] = run
	}

	// Grab the individual stats.
	res, err = dbconn.Query(`select dps.* from dp_run dpr join dp_stat dps on dpr.id = dps.dp_run_id where dpr.session_id = $1 order by dps.dp_run_id, dps.philosopher_number;`, code.SaveCode)
	if err != nil {
		return nil, err
	}
	for res.Next() {
		var id int64 = -1
		var stat = new(css.DPStat)
		if err := res.Scan(
			&id,
			&stat.PhilosopherNumber,
			&stat.Cycles,
			&stat.MaxHungry,
			&stat.MaxEating,
			&stat.MaxThinking,
			&stat.MinHungry,
			&stat.MinEating,
			&stat.MinThinking,
			&stat.TotalHungry,
			&stat.TotalEating,
			&stat.TotalThinking,
		); err != nil {
			panic(err)
		}
		runs[id].Stats = append(runs[id].Stats, stat)
	}

	result := new(css.DPStats)
	for _, v := range runs {
		result.DpRuns = append(result.DpRuns, v)
	}
	return result, nil
}

type ReadWriteStat struct {
	ActorType            string
	Cycles               int
	ActionTime, WaitTime db.Statistic
	Last                 int64
}

func storeUnisex(o css.UnisexOptions, stats []*db.UnisexStat) (err error) {
	slog.Info("Storing unisex stats")
	return runModel.StoreUnisex(o, stats)
}

func storeDp(o css.DPOptions, stats []*db.PhilosopherStat) error {
	slog.Info("Storing dp stats")
	return runModel.StoreDp(o, stats)
}
