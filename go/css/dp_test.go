package css

import (
	"log/slog"
	"os"
	"testing"
)

func TestNonSolution(t *testing.T) {
	o := DPOptions{
		Duration:     500000,
		Size:         5,
		TimeThinking: 1000,
		TimeEating:   1000,
	}
	log := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{AddSource: true, Level: slog.LevelDebug}))
	states := NonSolution(o, log)
	for range states {
	}
}

func TestNonSolutionLikelyDeadlock(t *testing.T) {
	o := DPOptions{
		Duration:     50000000000,
		Size:         4,
		TimeThinking: 0,
		TimeEating:   0,
	}
	log := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{AddSource: true, Level: slog.LevelDebug}))
	states := NonSolution(o, log)
	for range states {
	}
}
