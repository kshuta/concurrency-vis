package css

import (
	"context"
	"errors"
	"log"
	"log/slog"
	"sync"
	"time"
)

type dpSender chan *DPState

func (s dpSender) send(id int32, state DPState_State) {
	s.sendFork(id, state, 0)
}

func (s dpSender) sendFork(id int32, state DPState_State, forkID int32) {
	s <- &DPState{Id: id, State: state, Time: time.Now().UnixNano(), ForkId: forkID}
}

func left(id int32, size int32) int32 { return (id + 1) % size }
func right(id int32) int32            { return id }

func Philosophize(o *DPOptions, log *slog.Logger) (<-chan *DPState, error) {
	var c <-chan *DPState
	switch o.Choice {
	case 0:
		c = NonSolution(*o, log)
	case 1:
		c = Solution1(*o)
	case 2:
		c = Solution2(*o)
	default:
		return nil, errors.New("bad solution choice")
	}
	return c, nil
}

// NonSolution is the deadlocking solution for Dining Philosophers
// as presented in The Little Book of Semaphores.
func NonSolution(o DPOptions, log *slog.Logger) <-chan *DPState {
	log.Info("Running non solution")
	forkLock := &sync.Mutex{}
	forks := make([]chan bool, o.Size)
	pulse := make(chan int, o.Size*2)
	ctx, cancelCtx := context.WithTimeout(context.TODO(), time.Duration(o.Duration))

	for i := 0; i < len(forks); i++ {
		forks[i] = make(chan bool, 1)
		forks[i] <- true
	}

	pulseTL := time.Duration(o.TimeEating+o.TimeThinking) + time.Second
	go func() {
		timer := time.NewTimer(pulseTL)
		for {
			log.Debug("running select")
			select {
			case <-ctx.Done():
				log.Debug("time reached")
				return
			case <-timer.C:
				// check locks
				log.Debug("checking locks")
				count := int32(0)
				forkLock.Lock()
				for ; count < o.Size; count++ {
					if len(forks[count]) == 1 {
						break
					}
				}
				forkLock.Unlock()
				log.Debug("finished counting locks", "lockedCount", count, "actualCount", o.Size)
				if count < o.Size {
					timer.Reset(pulseTL)
					continue
				}
				cancelCtx()
				log.Debug("Deadlock detected, cancelling context")
				return
			case <-pulse:
				log.Debug("received pulse")
				timer.Stop()
				timer.Reset(pulseTL)
			}

		}
	}()

	return runSolutionAlt(
		ctx,
		o,
		func(id int32, c dpSender) {
			// get fork
			right := right(id)
			left := left(id, o.Size)
			hasRight, hasLeft := false, false

			c.sendFork(id, DPState_REQUESTING_FORK, right)
			c.sendFork(id, DPState_REQUESTING_FORK, left)

			log.Debug("getting fork", "philosopherId", id, "leftFork", left, "rightFork", right)
			for {
				select {
				case hasRight = <-forks[right]:
					c.sendFork(id, DPState_HOLDING_FORK, right)
					if hasRight && hasLeft {
						log.Debug("acquired fork", "philosopherId", id, "leftFork", left, "rightFork", right)
						return
					}
				case hasLeft = <-forks[left]:
					c.sendFork(id, DPState_HOLDING_FORK, left)
					if hasRight && hasLeft {
						log.Debug("acquired fork", "philosopherId", id, "leftFork", left, "rightFork", right)
						return
					}
				case <-ctx.Done():
					return
				}
			}
		},
		func(id int32, c dpSender) {
			// send fork
			defer func() {
				if err := recover(); err != nil {
					log.Error("panic occured: ", err)
				}
			}()
			right := right(id)
			left := left(id, o.Size)
			log.Debug("sending fork", "philospherId", id, "leftFork", left, "rightFork", right)

			forkLock.Lock()
			forks[right] <- true
			c.sendFork(id, DPState_RELEASING_FORK, right)
			forks[left] <- true
			c.sendFork(id, DPState_RELEASING_FORK, left)
			forkLock.Unlock()
		},
		pulse,
		log,
	)
}

// Solution1 is the first solution from The Little Book of Semaphores
// which is not deadlocking.
func Solution1(o DPOptions) <-chan *DPState {
	forks := make([]sync.Mutex, o.Size)
	footman := make(chan unitType, o.Size-1)
	for i := int32(0); i < o.Size-1; i++ {
		footman <- unit
	}
	return runSolution(
		o,
		func(id int32, c dpSender) {
			right := right(id)
			left := left(id, o.Size)
			<-footman
			c.sendFork(id, DPState_REQUESTING_FORK, right)
			forks[right].Lock()
			c.sendFork(id, DPState_HOLDING_FORK, right)

			c.sendFork(id, DPState_REQUESTING_FORK, left)
			forks[left].Lock()
			c.sendFork(id, DPState_HOLDING_FORK, left)
		},
		func(id int32, c dpSender) {
			right := right(id)
			left := left(id, o.Size)
			c.sendFork(id, DPState_RELEASING_FORK, right)
			forks[right].Unlock()
			c.sendFork(id, DPState_RELEASING_FORK, left)
			forks[left].Unlock()
			footman <- unit
		},
		new(bool),
	)
}

// Solution2 is the second non-deadlocking solution as presented
// in The Little Book of Semaphores where some of the philosophers
// are lefties.
func Solution2(o DPOptions) <-chan *DPState {
	forks := make([]sync.Mutex, o.Size)
	return runSolution(
		o,
		func(id int32, c dpSender) {
			right := right(id)
			left := left(id, o.Size)
			var first, second int32
			if id == 0 {
				first = left
				second = right
			} else {
				first = right
				second = left
			}
			c.sendFork(id, DPState_REQUESTING_FORK, first)
			forks[first].Lock()
			c.sendFork(id, DPState_HOLDING_FORK, first)

			c.sendFork(id, DPState_REQUESTING_FORK, second)
			forks[second].Lock()
			c.sendFork(id, DPState_HOLDING_FORK, second)
		},
		func(id int32, c dpSender) {
			right := right(id)
			left := left(id, o.Size)
			var first, second int32
			if id == 0 {
				first = left
				second = right
			} else {
				first = right
				second = left
			}
			c.sendFork(id, DPState_RELEASING_FORK, first)
			forks[first].Unlock()
			c.sendFork(id, DPState_RELEASING_FORK, second)
			forks[second].Unlock()
		},
		new(bool),
	)
}

func runSolutionAlt(ctx context.Context, o DPOptions, getForks, putForks func(int32, dpSender), pulse chan int, log *slog.Logger) <-chan *DPState {
	log.Debug("Running Alt Solution", "Choice", o.Choice, "Duration", o.Duration, "Size", o.Size, "TimeThinking", o.TimeThinking, "TimeEating", o.TimeEating, "Varation", o.Variation)
	c, done := make(dpSender, 16384), make(chan bool)
	for i := int32(0); i < o.Size; i++ {
		go func(id int32) {
			for {
				select {
				case <-ctx.Done():
					if errors.Is(ctx.Err(), context.Canceled) {
						log.Debug("exiting with deadlock", "id", id)
						c.send(id, DPState_DEADLOCKED)
					} else {
						c.send(id, DPState_GRACEFULLY_EXITED)
						log.Info("sending done from timeout", "id", id)
					}
					done <- true
					return
				default:
					log.Debug("sending pulse", "id", id)
					pulse <- 1
					sleepRange(o.TimeThinking, o.Variation)
					if ctx.Err() != nil {
						break
					}
					c.send(id, DPState_HUNGRY)
					getForks(id, c)
					if ctx.Err() != nil {
						break
					}
					c.send(id, DPState_EATING)
					sleepRange(o.TimeEating, o.Variation)
					if ctx.Err() != nil {
						break
					}
					putForks(id, c)
					if ctx.Err() != nil {
						break
					}
					c.send(id, DPState_THINKING)
				}
			}
		}(i)
	}
	go func() {
		for i := int32(0); i < o.Size; i++ {
			<-done
			log.Debug("received done", "doneCount", i)
		}
		close(c)
		log.Info("channel closed from source")
	}()
	return c
}

func runSolution(o DPOptions, getForks, putForks func(int32, dpSender), deadlocked *bool) <-chan *DPState {
	log.Println("Running solution with following options: ")
	log.Printf("Choice: %d\n", o.Choice)
	log.Printf("Duration: %d\n", o.Duration)
	log.Printf("Size: %d\n", o.Size)
	log.Printf("TimeThinking: %d\n", o.TimeThinking)
	log.Printf("TimeEating: %d\n", o.TimeEating)
	log.Printf("Variation: %d\n", o.Variation)

	c, done := make(dpSender, 16384), make(chan bool)
	for i := int32(0); i < o.Size; i++ {
		go func(id int32) {
			timeout := time.After(time.Duration(o.Duration))
			for {
				select {
				case <-timeout:
					c.send(id, DPState_GRACEFULLY_EXITED)
					log.Printf("sending done from timeout: %d", id)
					done <- true
					return
				default:
					sleepRange(o.TimeThinking, o.Variation)
					c.send(id, DPState_HUNGRY)
					getForks(id, c)
					if *deadlocked {
						c.send(id, DPState_DEADLOCKED)
						log.Printf("sending done after get fork: %d", id)
						done <- true
						return
					}
					c.send(id, DPState_EATING)
					sleepRange(o.TimeEating, o.Variation)
					c.send(id, DPState_THINKING)
					if *deadlocked {
						c.send(id, DPState_DEADLOCKED)
						log.Printf("sending done after sleep: %d", id)
						done <- true
						return
					}
					putForks(id, c)
					if *deadlocked {
						c.send(id, DPState_DEADLOCKED)
						log.Printf("sending done after put fork: %d", id)
						done <- true
						return
					}
				}
			}
		}(i)
	}
	go func() {
		for i := int32(0); i < o.Size; i++ {
			<-done
			log.Printf("received done %d\n", i)
		}
		close(c)
		log.Println("channel closed from source")
	}()
	return c
}
