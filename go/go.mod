module gitlab.com/kshuta/concurrency-vis

go 1.21

require (
	github.com/Netflix/go-env v0.0.0-20220526054621-78278af1949d
	github.com/golang/protobuf v1.4.2
	github.com/joho/godotenv v1.5.1
	github.com/julienschmidt/httprouter v1.3.0
	github.com/lib/pq v1.7.0
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
	google.golang.org/grpc v1.30.0
)

require (
	golang.org/x/net v0.0.0-20190311183353-d8887717615a // indirect
	golang.org/x/sys v0.0.0-20190215142949-d0b11bdaac8a // indirect
	golang.org/x/text v0.3.0 // indirect
	google.golang.org/genproto v0.0.0-20200624020401-64a14ca9d1ad // indirect
	google.golang.org/protobuf v1.25.0 // indirect
)
