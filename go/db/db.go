package db

import (
	"database/sql"
	"errors"
	"fmt"
	"log/slog"
	"strconv"
	"time"

	"gitlab.com/kshuta/concurrency-vis/css"
)

type RunModel struct {
	db *sql.DB
}

type UnisexStat struct {
	ActorType            string
	Cycles               int
	ActionTime, WaitTime Statistic
	Last                 int64
}

type BufferStat struct {
	ActorType            string
	Cycles               int
	ActionTime, WaitTime Statistic
	Last                 int64
}

// PhilosopherStat is a set of statistics for how an individual philosopher
// performed during a run.
type PhilosopherStat struct {
	Eating, Thinking, Hungry Statistic
	Cycles                   int
	Last                     int64
	Deadlocked               bool
}

type Statistic struct {
	Min, Max, Total int64
}

func NewRunModel(db *sql.DB) *RunModel {
	return &RunModel{db}
}

func (r *RunModel) Close() error {
	return r.db.Close()
}

func (r *RunModel) GetSaveCode() (*css.SaveCode, error) {
	var saveCode css.SaveCode
	const query = `insert into session default values returning id`
	row := r.db.QueryRow(query)
	err := row.Scan(&saveCode.SaveCode)

	if err != nil {
		return nil, err
	}

	return &saveCode, nil
}

func (r *RunModel) ValidateSaveCode(saveCode string) (bool, error) {
	const query = `select exists(select 1 from session where id = $1)`
	var exists bool
	err := r.db.QueryRow(query, saveCode).Scan(&exists)

	if err != nil {
		return false, err
	}

	return exists, nil
}

func (r *RunModel) SaveBufferToDB(o css.BufferOptions, stats []*BufferStat) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	row := tx.QueryRow(
		`insert into buffer_run (session_id, timestamp, duration, buffer_size, num_producers, num_consumers, produce_time, consume_time, variation, sleep_time) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10) returning id`,
		o.SaveCode.SaveCode,
		time.Now(),
		o.Duration,
		o.BufferSize,
		o.NumProducers,
		o.NumConsumers,
		o.ProduceTime,
		o.ConsumeTime,
		o.Variation,
		o.SleepTime,
	)

	var id int64
	if err = row.Scan(&id); err != nil {
		_ = tx.Rollback()
		return err
	}

	for num, v := range stats {
		_, err = tx.Exec(
			`insert into buffer_stat (buffer_run_id, buffer_number, actor_type, cycles, max_action_time, max_wait_time, min_action_time, min_wait_time, total_action_time, total_wait_time) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`,
			id,
			num,
			v.ActorType,
			v.Cycles,
			v.ActionTime.Max,
			v.WaitTime.Max,
			v.ActionTime.Min,
			v.WaitTime.Min,
			v.ActionTime.Total,
			v.WaitTime.Total,
		)
		if err != nil {
			_ = tx.Rollback()
			return err
		}
	}
	return tx.Commit()
}

func (stat *BufferStat) Update(s *css.BufferState) {
	delta := s.Time - stat.Last
	var statistic *Statistic
	if s.State == css.BufferState_PRODUCED_ITEM || s.State == css.BufferState_CONSUMED_ITEM {
		statistic = &stat.ActionTime
		stat.Cycles++
	} else {
		statistic = &stat.WaitTime
	}

	stat.Last = s.Time
	statistic.Total += delta
	if statistic.Min == 0 || delta < statistic.Min {
		statistic.Min = delta
	}
	if delta > statistic.Max {
		statistic.Max = delta
	}
}

func (r *RunModel) GetUnisexRun(code, timestamp string) (*css.UnisexRun, error) {

	t, err := strconv.ParseInt(timestamp, 10, 64)
	if err != nil {
		return nil, fmt.Errorf("failed to parse timestamp: %w", err)
	}

	res := r.db.QueryRow(`select * from unisex_run ur where ur.session_id = $1 and EXTRACT(EPOCH from ur.timestamp)*1000000000 = $2`, code, t)
	var id int64
	var run = new(css.UnisexRun)
	run.Options = new(css.UnisexOptions)
	run.Options.SaveCode = new(css.SaveCode)
	var timeValue time.Time
	if err := res.Scan(
		&id,
		&(run.Options.SaveCode.SaveCode),
		&timeValue,
		&(run.Options.Duration),
		&(run.Options.Solution),
		&run.Options.NumMales,
		&(run.Options.NumFemales),
		&(run.Options.FemaleTime),
		&(run.Options.MaleTime),
		&(run.Options.Variation),
		&(run.Options.RestartTime),
		&(run.Options.MaxAllowed),
	); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, fmt.Errorf("no row with specified condition: %w", err)
		} else {
			return nil, fmt.Errorf("error reading row: %w", err)
		}
	}

	// Grab the individual stats.
	stats, err := r.db.Query(`select us.* from unisex_run ur join unisex_stat us on ur.id = us.unisex_run_id where ur.session_id = $1 and EXTRACT(EPOCH from timestamp)*1000000000 = $2  order by us.unisex_run_id, us.unisex_number;`, code, t)
	if err != nil {
		return nil, fmt.Errorf("error querying multiple rows: %w", err)
	}
	defer stats.Close()
	for stats.Next() {
		var id int64 = -1
		var throwaway string
		var stat = new(css.UnisexRunStat)
		if err := stats.Scan(
			&id,
			&stat.Id,
			&throwaway,
			&stat.TimesUsed,
			&stat.TimeUsingMax,
			&stat.WaitTimeMax,
			&stat.TimeUsingMin,
			&stat.WaitTimeMin,
			&stat.TimeUsingTotal,
			&stat.WaitTimeTotal,
		); err != nil {
			panic(err)
		}
		run.Stats = append(run.Stats, stat)
	}

	return run, nil

}

func (r *RunModel) GetUnisexStats(code string) (*css.UnisexStats, error) {
	res, err := r.db.Query(`select * from unisex_run ur where ur.session_id = $1 order by ur.id desc`, code)
	if err != nil {
		return nil, err
	}
	defer res.Close()

	// Grab runs
	var runs = make(map[int64]*css.UnisexRun)
	for res.Next() {
		var id int64
		var run = new(css.UnisexRun)
		run.Options = new(css.UnisexOptions)
		run.Options.SaveCode = new(css.SaveCode)
		var timeValue time.Time
		if err := res.Scan(
			&id,
			&(run.Options.SaveCode.SaveCode),
			&timeValue,
			&(run.Options.Duration),
			&(run.Options.Solution),
			&run.Options.NumMales,
			&(run.Options.NumFemales),
			&(run.Options.FemaleTime),
			&(run.Options.MaleTime),
			&(run.Options.Variation),
			&(run.Options.RestartTime),
			&(run.Options.MaxAllowed),
		); err != nil {
			return nil, err
		}
		run.Timestamp = timeValue.UnixNano()
		runs[id] = run
	}

	// Grab the individual stats.
	res, err = r.db.Query(`select us.* from unisex_run ur join unisex_stat us on ur.id = us.unisex_run_id where ur.session_id = $1 order by us.unisex_run_id, us.unisex_number;`, code)
	if err != nil {
		return nil, err
	}
	defer res.Close()
	for res.Next() {
		var id int64 = -1
		var throwaway string
		var stat = new(css.UnisexRunStat)
		if err := res.Scan(
			&id,
			&stat.Id,
			&throwaway,
			&stat.TimesUsed,
			&stat.TimeUsingMax,
			&stat.WaitTimeMax,
			&stat.TimeUsingMin,
			&stat.WaitTimeMin,
			&stat.TimeUsingTotal,
			&stat.WaitTimeTotal,
		); err != nil {
			panic(err)
		}
		runs[id].Stats = append(runs[id].Stats, stat)
	}

	result := new(css.UnisexStats)
	for _, v := range runs {
		result.UnisexRuns = append(result.UnisexRuns, v)
	}
	return result, nil

}

func (r *RunModel) StoreUnisex(o css.UnisexOptions, stats []*UnisexStat) (err error) {
	tx, err := r.db.Begin()
	slog.Info("unisex options", "options", o)
	defer func() {
		if err == nil {
			slog.Debug("Committing changes")
			err = tx.Commit()
		} else {
			slog.Error("Error saving unisex, rolling back:", err)
			_ = tx.Rollback()
		}
	}()
	if err != nil {
		return
	}
	res := r.db.QueryRow(
		"insert into unisex_run (session_id, timestamp, duration, solution, num_males, num_females, female_time, male_time, variation, sleep_time, capacity) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11) returning id",
		o.SaveCode.SaveCode,
		time.Now(),
		o.Duration,
		o.Solution,
		o.NumMales,
		o.NumFemales,
		o.FemaleTime,
		o.MaleTime,
		o.Variation,
		o.RestartTime,
		o.MaxAllowed,
	)
	var id int
	err = res.Scan(&id)
	if err != nil {
		return
	}
	for i, stat := range stats {
		_, err := r.db.Exec("insert into unisex_stat (unisex_run_id, unisex_number, actor_type, cycles, max_action_time, max_wait_time, min_action_time, min_wait_time, total_action_time, total_wait_time) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)",
			id,
			i,
			stat.ActorType,
			stat.Cycles,
			stat.ActionTime.Max,
			stat.WaitTime.Max,
			stat.ActionTime.Min,
			stat.WaitTime.Min,
			stat.ActionTime.Total,
			stat.WaitTime.Total,
		)
		if err != nil {
			return err
		}
	}
	return nil
}

func (stat *UnisexStat) Update(state *css.UnisexState) {
	delta := state.Time - stat.Last

	var statistic *Statistic
	if state.State == css.UnisexState_ENTERING {
		statistic = &stat.WaitTime
		stat.Cycles++
	} else if state.State == css.UnisexState_LEAVING {
		statistic = &stat.ActionTime
	}

	if statistic == nil {
		return
	}
	stat.Last = state.Time
	statistic.Total += delta
	if statistic.Min == 0 || delta < statistic.Min {
		statistic.Min = delta
	}
	if delta > statistic.Max {
		statistic.Max = delta
	}
}

func (r *RunModel) StoreDp(o css.DPOptions, stats []*PhilosopherStat) (err error) {
	tx, err := r.db.Begin()
	defer func() {
		if err == nil {
			err = tx.Commit()
		} else {
			_ = tx.Rollback() // TODO maybe handle this idk
		}
	}()
	if err != nil {
		return
	}
	res := r.db.QueryRow(
		"insert into dp_run (session_id, timestamp, duration, num_philosophers, think_time, eat_time, variation, deadlocked, solution) values ($1, $2, $3, $4, $5, $6, $7, $8, $9) returning id",
		o.SaveCode.SaveCode,
		time.Now(),
		o.Duration,
		o.Size,
		o.TimeThinking,
		o.TimeEating,
		o.Variation,
		stats[0].Deadlocked,
		o.Choice,
	)
	var id int
	err = res.Scan(&id)
	if err != nil {
		return
	}
	for i, stat := range stats {
		_, err := r.db.Exec("insert into dp_stat (dp_run_id, philosopher_number, cycles, max_hungry, max_eating, max_thinking, min_hungry, min_eating, min_thinking, total_hungry, total_eating, total_thinking) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)",
			id,
			i,
			stat.Cycles,
			stat.Hungry.Max,
			stat.Eating.Max,
			stat.Thinking.Max,
			stat.Hungry.Min,
			stat.Eating.Min,
			stat.Thinking.Min,
			stat.Hungry.Total,
			stat.Eating.Total,
			stat.Thinking.Total,
		)
		if err != nil {
			return err
		}
	}
	return
}

func (stat *PhilosopherStat) Update(s *css.DPState) {
	var statistic *Statistic
	delta := s.Time - stat.Last
	if s.State == css.DPState_HUNGRY {
		statistic = &stat.Thinking
	} else if s.State == css.DPState_EATING {
		statistic = &stat.Hungry
	} else if s.State == css.DPState_THINKING {
		stat.Cycles++
		statistic = &stat.Eating
	} else if s.State == css.DPState_DEADLOCKED {
		stat.Deadlocked = true
		return
	}
	if statistic == nil {
		return
	}
	stat.Last = s.Time
	if statistic.Min == 0 || delta < statistic.Min {
		statistic.Min = delta
	}
	if delta > statistic.Max {
		statistic.Max = delta
	}
	statistic.Total += delta
}
