# Concurrency Simulations

### An interactive tool for visualization of classic concurrency problems

This repository has been forked from the MSE capstone project for Adam Yakes, to make the tool available for the UWL students.

The goal of this project was to create an educational tool for use in a Computer Science course teaching concurrency. It uses problems and solutions from [The hLittle Book of Semaphores](https://greenteapress.com/wp/semaphores/) by Allen B. Downey, and creates visualizations of those problems. Users can parameterize the runs of the simulation to change its behavior and see how different inputs can affect throughput and fairness. After a run is completed, summary information is saved which gives numerical data about the qualities shown.

The project is built with [Angular](https://angular.io/) and has a backend written in [Go](https://go.dev/), uses [gRPC](https://grpc.io/) to communicate in live time, has animations written in [SVG.js](https://svgjs.com/docs/3.0/), and saves past run information in a [PostgreSQL](https://www.postgresql.org/) database.

Details on developing the app can be found under `DEVELOPMENT.md`. Details on deploying the application can be found under `DEPLOYMENT.md`
