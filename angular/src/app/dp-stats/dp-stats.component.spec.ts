import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { DpStatsComponent } from './dp-stats.component';

describe('DpStatsComponent', () => {
  let component: DpStatsComponent;
  let fixture: ComponentFixture<DpStatsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DpStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DpStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
