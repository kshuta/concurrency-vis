import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { BoundedBufferComponent } from './bounded-buffer.component';

describe('BoundedBufferComponent', () => {
  let component: BoundedBufferComponent;
  let fixture: ComponentFixture<BoundedBufferComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ BoundedBufferComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoundedBufferComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
