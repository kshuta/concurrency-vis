export enum Problems {
    DiningPhilosophers,
    ReadWrite,
    BoundedBuffer,
    UnisexBathroom,
}
